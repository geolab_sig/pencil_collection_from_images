import base64
import os
import xml.dom.minidom as minidom
import imghdr
from PIL import Image

class imgDirToPencil:
    author      = 'Arnaud Vandecasteele (arnaud[dot]sig[at]gmail[dot]com)'
    description = 'A special map theme for pencil - Icone from http://projects.opengeo.org/geosilk/'
    displayName = 'MapIcons'
    idFile      = 'pencilMap'
    url         = 'http://projects.opengeo.org/geosilk/'   
    dirImgs     = '/home/arnaud/Documents/pencil_map/Icons'
    
    def __init__(self):
        definitionpath = os.path.join(self.dirImgs, "..", "Definition.xml")
        #delete previous Definition.xml if existe
        if os.path.isfile(definitionpath):
            os.remove(definitionpath) 
       
        #create xml entry for each img
        result, doc  = self.__createShapeListFromImg()
        if(result):
            #save xml file
            xmlResultFile = open(definitionpath, 'w')
            xmlResultFile.write(doc.toprettyxml())
            xmlResultFile.close()
        else:
            print doc

    def __encodeImgBase64(self, imgpath):
        img = open(imgpath, "rb")
        base64str = base64.b64encode(img.read())
        img.close()
        return base64str

    def __createShapeListFromImg(self):
        #Create xml file
        doc = minidom.Document()
        shapes = doc.createElement("Shapes")
        shapes.setAttribute('xmlns', 'http://www.evolus.vn/Namespace/Pencil') 
        shapes.setAttribute('xmlns:p', 'http://www.evolus.vn/Namespace/Pencil') 
        shapes.setAttribute('xmlns:svg', 'http://www.w3.org/2000/svg')
        shapes.setAttribute('xmlns:xlink', 'http://www.w3.org/1999/xlink') 
        shapes.setAttribute('author', self.author)
        shapes.setAttribute('id', self.idFile)
        shapes.setAttribute('displayName', self.displayName)
        shapes.setAttribute('description', self.description)
        shapes.setAttribute('url', self.url)
        doc.appendChild(shapes)

        imgFileFound =   0
        for dirpath, subdirs, files in os.walk(self.dirImgs):             
            for imgfile in files:                                    
                imgpath = os.path.join(dirpath,imgfile)                
                try:                        
                    pilImg = Image.open(imgpath)
                    w, h = pilImg.size              
                    imgRelPath = os.path.relpath(os.path.realpath(os.path.join(dirpath, imgfile)), os.path.realpath(__file__))[3:]
                    #for each images add xml info into the main xml file
                    shape = doc.createElement('Shape')
                    idname = os.path.splitext(imgfile)[0]
                    name = idname.replace('_',' ')    
                    shape.setAttribute('id', idname)
                    shape.setAttribute('displayName', name)
                    shape.setAttribute('icon', imgRelPath)

                    p = doc.createElement('p:Content')
                    p.setAttribute('xmlns:p','http://www.evolus.vn/Namespace/Pencil')
                    p.setAttribute('xmlns','http://www.w3.org/2000/svg')            

                    img = doc.createElement('image')
                    img.setAttribute('id', 'image')
                    img.setAttribute('height', '{0}'.format(h))
                    img.setAttribute('width', '{0}'.format(w))
                    img.setAttribute('xlink:href', "data:image/png;base64," + self.__encodeImgBase64(imgpath))
                    
                    p.appendChild(img)
                    shape.appendChild(p)
                    shapes.appendChild(shape)
                    imgFileFound += 1
                except IOError:
                    print "cannot open", imgpath  
               
        if imgFileFound:        
            return (True, doc) 
        else:
            return (False, "No image file in the directory")

imgDirToPencil()

